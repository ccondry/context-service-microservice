const hydraExpress = require('hydra-express')
const hydra = hydraExpress.getHydra()
const CS = require('context-service-rest-client')
const pkg = require('./package.json')
// load environment file
require('dotenv').load()

const cs = new CS(process.env.rest_uri_base)

// set up hydra and redis config
const hydraConfig = {
  hydra: {
    serviceName: pkg.name,
    serviceIP: process.env.hydra_service_ip || '',
    servicePort: process.env.hydra_service_port || 0,
    serviceType: process.env.hydra_service_type || '',
    serviceDescription: pkg.description,
    redis: {
      url: process.env.redis_url,
      port: process.env.redis_port,
      db: process.env.redis_db
    }
  }
}

function processError (res, response) {
  if (response.status > 0 && response.status < 600 && response.status !== 404) {
    // non-404, so return the status
    try {
      res.status(response.status).send({message: response.error.error})
    } catch (e) {
      try {
        res.status(response.status).send({message: response.message})
      } catch (e2) {
        return res.status(500).send({message: 'Server Error'})
      }
    }
  } else {
    // 404 means the Context Service web app tomcat is up
    // but the web app failed to load properly
    return res.status(503).send({message: 'Service Unavailable'})
  }
}

// define routes
function onRegisterRoutes() {
  const express = hydraExpress.getExpress()
  const api = express.Router()

  // retrieve
  api.get('/:type/:id', async function(req, res) {
    const type = req.params.type
    const id = req.params.id
    try {
      console.log(`retrieving Context Service ${type} with ID = ${id}...`)
      const results = await cs.get(type, id)
      console.log(`successfully retreived Context Service ${type} with ID = ${id}.`)
      res.status(200).send({results})
    } catch (response) {
      console.log(response)
      return processError(res, response)
    }
  })

  // create
  api.post('/', async function(req, res) {
    const type = req.body.type
    try {
      console.log(`creating Context Service ${type}...`)
      const id = await cs.create(req.body)
      console.log(`successfully created Context Service ${type}. ID = ${id}`)
      // create location header to match this service's GET route
      const location = `${hydraConfig.hydra.serviceName}:[get]/${type}/${id}`
      // return location in header and id in the body. yes, even though it is 201.
      res.status(201).set({location}).send({id})
    } catch (response) {
      console.log(`failed to create Context Service ${type}`)
      return processError(res, response)
    }
  })

  // delete
  api.delete('/:type/:id', async function(req, res) {
    const type = req.params.type
    const id = req.params.id
    try {
      console.log(`deleting Context Service ${type} with ID = ${id}...`)
      await cs.delete(type, id)
      console.log(`successfully deleted Context Service ${type} with ID ${id}.`)
      // ACCEPTED
      res.status(202).send({})
    } catch (response) {
      console.log(`failed to delete Context Service ${type} with ID = ${id}.`)
      return processError(res, response)
    }
  })

  // update
  api.put('/:type/:id', async function(req, res) {
    const type = req.params.type
    const id = req.params.id
    const body = req.body
    try {
      console.log(`updating Context Service ${type} with ID = ${id}...`)
      await cs.update(type, id, body)
      console.log(`successfully updated Context Service ${type} with ID = ${id}`)
      // ACCEPTED
      res.status(202).send()
    } catch (response) {
      console.log(`failed to update Context Service ${type}`)
      return processError(res, response)
    }
  })

  // search
  api.post('/search/:type', async function(req, res) {
    const type = req.params.type
    const query = req.body.query
    const operation = req.body.operation
    try {
      console.log(`searching for Context Service ${type}s...`)
      const results = await cs.search(type, query, operation)
      console.log(`successfully searched for Context Service ${type}s. found ${results.length} results.`)
      res.status(200).send({results})
    } catch (response) {
      console.log(`failed to search for Context Service ${type}s`)
      return processError(res, response)
    }
  })

  hydraExpress.registerRoutes({
    '': api
  })
}

// start up
hydraExpress.init(hydraConfig, onRegisterRoutes)
.then(serviceInfo => {
  console.log('serviceInfo', serviceInfo)
  // return 0
})
.catch(err => console.log('err', err))
